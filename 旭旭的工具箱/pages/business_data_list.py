'''
Desc: 
Author: 马大胖
Mail: mazhenjun.lk.xx@gmail.com
Date: 2024-08-19 09:45:19
'''
import tkinter as tk
from tkinter import scrolledtext, ttk
from functions.business_data_list_function import getCookie, submit


def cloud_network_find_business_data_list(frame, clear_frame):
    clear_frame(frame)
    
    # 接入号标签和文本域
    access_number_label = tk.Label(frame, text="接入号(用换行分隔):")
    access_number_label.place(x=10, y=10)

    access_number_text = tk.Text(frame, height=5, width=50)
    access_number_text.place(x=150, y=10, width=600, height=100)

    # Cookie标签和文本域
    cookie_label = tk.Label(frame, text="Cookie:")
    cookie_label.place(x=10, y=120)

    cookie_text = tk.Text(frame, height=5, width=50)
    cookie_text.place(x=150, y=120, width=600, height=100)

    # 获取Cookie按钮
    getCookie_button = tk.Button(frame, text="获取Cookie", command=getCookie, state="disabled")
    getCookie_button.place(x=150, y=230, width=300, height=30)

    # 提交按钮
    submit_button = tk.Button(frame, text="提交", command=submit)
    submit_button.place(x=460, y=230, width=300, height=30)

    # 日志显示区域
    log_label = tk.Label(frame, text="日志:")
    log_label.place(x=10, y=270)
    log_text = scrolledtext.ScrolledText(frame, height=20, state="disabled", bg="lightgray", width=30)
    log_text.place(x=10, y=300, width=350, height=250)

    # 结果显示区域
    result_label = tk.Label(frame, text="结果:")
    result_label.place(x=370, y=270)
    result_text = scrolledtext.ScrolledText(frame, height=20, wrap=tk.NONE)
    result_text.place(x=370, y=300, width=750, height=250)

    # 水平滚动条的配置
    h_scroll = ttk.Scrollbar(frame, orient=tk.HORIZONTAL, command=result_text.xview, style="TScrollbar")
    result_text.configure(xscrollcommand=h_scroll.set)
    h_scroll.place(x=370, y=550, width=750)

    # 进度条
    progress_label = tk.Label(frame, text="进度: 0/0")
    progress_label.place(x=10, y=560)

def clear_frame(frame):
    for widget in frame.winfo_children():
        widget.destroy()