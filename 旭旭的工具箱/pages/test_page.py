'''
Desc: 
Author: 马大胖
Mail: mazhenjun.lk.xx@gmail.com
Date: 2024-08-19 09:52:48
'''
from tkinter import Label

def show_page2(frame, clear_frame):
    clear_frame(frame)
    label = Label(frame, text="这是页面 22223333")
    label.pack()

def clear_frame(frame):
    for widget in frame.winfo_children():
        widget.destroy()