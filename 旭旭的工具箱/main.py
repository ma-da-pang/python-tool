import tkinter as tk
from tkinter import Menu, Frame
from pages.business_data_list import cloud_network_find_business_data_list
from pages.test_page import show_page2

def clear_frame(frame):
    for widget in frame.winfo_children():
        widget.destroy()

# GUI部分
root = tk.Tk()
root.title("旭旭小仙女的工具箱")
root.geometry("1200x600")

# 创建菜单栏
menu_bar = Menu(root)
root.config(menu=menu_bar)

# 创建"功能"菜单
function_menu = Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="新一代云网", menu=function_menu)

# 添加菜单项并绑定相应的功能
function_menu.add_command(label="接入号关联查询", command=lambda: cloud_network_find_business_data_list(frame, clear_frame))
function_menu.add_command(label="测试页面", command=lambda: show_page2(frame, clear_frame))

# 创建一个容器 Frame，用于显示不同的页面
frame = Frame(root)
frame.pack(fill="both", expand=True)

# 初始显示接入号关联查询
cloud_network_find_business_data_list(frame, clear_frame)

# 启动主循环
root.mainloop()