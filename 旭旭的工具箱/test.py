from selenium import webdriver

# 设置 Selenium WebDriver（以 Chrome 为例）
driver = webdriver.Chrome()

# 访问登录页面
driver.get('https://mail.163.com/')

# 填写登录表单
#username_input = driver.find_element('name', 'username')
#password_input = driver.find_element('name', 'password')
#login_button = driver.find_element('xpath', '//*[@id="loginBtn"]')

#username_input.send_keys('123123123123')
#password_input.send_keys('12312312312')
#login_button.click()

# 等待页面加载，确保登录成功
driver.implicitly_wait(5)

# 获取当前的会话 Cookie
cookies = driver.get_cookies()
print(cookies)  # 打印所有的 Cookie

# 继续访问其他页面，这些页面将自动携带当前会话的 Cookie
# driver.get('https://example.com/protected-page')

# 操作完毕后关闭浏览器
driver.quit()
