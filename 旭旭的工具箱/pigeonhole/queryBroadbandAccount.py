'''
Desc: 
Author: 马大胖
Mail: mazhenjun.lk.xx@gmail.com
Date: 2024-08-16 16:21:21
'''


import tkinter as tk
from tkinter import scrolledtext
import requests
import logging

# 设置日志记录
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# 接口URL
find_todo_box_url = "http://136.64.71.234:30121/aiops/api/v1/flow/tj/constructionWorkTable/getGzConstructionInfo"
find_formIdAndPid_url = "http://136.64.71.234:30121/aiops/api/v1/form/getFlowForm"
find_broadband_info_url = "http://136.64.71.234:30121/aiops/api/v1/form/getFormData"

# Base payload
base_payload = {
    "accessNo": "",
    "type": "B",
    "page": 0,
    "pageSize": 12
}

# 设置代理
proxies = {
    "http": "http://136.64.71.234:30121",
    "https": "http://136.64.71.234:30121"
}

# 最终的数据vo对象
final_result = {}

def call_api_and_extract_attributes(access_code, headers):
    payload = base_payload.copy()
    payload['accessNo'] = access_code

    try:
        response = requests.post(find_todo_box_url, headers=headers, json=payload, proxies=proxies)

        if response.status_code == 200:
            logging.info(f"成功获取 接入号: {access_code} 的基础数据")
            return response.json()
        else:
            logging.error(f"获取 接入号: {access_code} 的基础数据失败, 状态码: {response.status_code}, 响应内容: {response.text}")
            return None
    except requests.RequestException as e:
        logging.error(f"请求失败: {e}")
        return None

def extract_ids_with_product_type(response):
    address_url_accessNo_list = []
    if response and "content" in response:
        for key in response.get("content", []):
            result_param = {
                "flow_id": key.get("flow_id"),
                "access_no": key.get("access_no"),
                "address": key.get("address"),
                "url": key.get("url")
            }
            address_url_accessNo_list.append(result_param)
            final_result[key.get("access_no")] = result_param
    return address_url_accessNo_list

def getFormIdAndPid(step_one_result, headers):
    result = []
    for step_two in step_one_result:
        params = {'flowId': step_two.get("flow_id")}
        try:
            response = requests.get(find_formIdAndPid_url, headers=headers, params=params, proxies=proxies)

            if response.status_code == 200:
                logging.info(f"成功获取 接入号: {step_two.get('access_no')} 的 Form-Pid 数据")
                result.append(response.json())
            else:
                logging.error(f"获取 接入号: {step_two.get('access_no')} 的 Form-Pid 数据失败, 状态码: {response.status_code}, 响应内容: {response.text}")
                return None
        except requests.RequestException as e:
            logging.error(f"请求失败: {e}")
    return result

def getBroadbandData(final_result, headers):
    result = []
    for step_three in final_result:
        temp = final_result[step_three]
        params = {'formId': temp.get("formId"), "pId": temp.get("pId"), "pageNum": 0, "pageSize": 5}
        try:
            response = requests.get(find_broadband_info_url, headers=headers, params=params, proxies=proxies)

            if response.status_code == 200:
                logging.info(f"成功获取 接入号: {temp.get('access_no')} 的宽带详情数据")
                result.append(response.json())
            else:
                logging.error(f"获取 接入号: {temp.get('access_no')} 的宽带详情数据失败")
        except requests.RequestException as e:
            logging.error(f"请求失败: {e}")
    return result

def remove_duplicates(arr):
    return list(set(arr))

# GUI部分
def submit():
    access_codes = access_number_text.get("1.0", tk.END).strip().split(',')
    cookie = cookie_text.get("1.0", tk.END).strip()

    headers = {
        "Content-Type": "application/json;charset=UTF-8",
        "Cookie": cookie
    }

    for access_code in access_codes:
        response = call_api_and_extract_attributes(access_code.strip(), headers)
        if response:
            step_one_result = extract_ids_with_product_type(response)
            step_two_result = getFormIdAndPid(step_one_result, headers)

            if step_two_result:
                for result_temp in step_two_result:
                    formId = ""
                    access_no = ""
                    pId = ""
                    for table in result_temp.get("table", {}):
                        for t_tmp in table.get("field", {}):
                            if t_tmp.get("fieldName") == "REQUEST_ID":
                                pId = t_tmp.get("value")
                            if t_tmp.get("fieldName") == "access_no":
                                access_no = t_tmp.get("value")
                            if t_tmp.get("fieldName") == "product_list_tab":
                                a = t_tmp.get("value")
                                formId = a.get("formId")
                    b = final_result[access_no]
                    b["formId"] = formId
                    b["pId"] = pId

                fff = getBroadbandData(final_result, headers)
                if len(fff) > 0:
                    one = []
                    for result_temp in fff:
                        two = []
                        a = result_temp.get("data")
                        for a_temp in a:
                            two.append(a_temp.get("accessNo"))
                        one.append(two)

                    for a in one:
                        a_t = []
                        f_t = []
                        a_unique = remove_duplicates(a)
                        for bbb in a_unique:
                            ccc = final_result.get(bbb, None)
                            if ccc is not None:
                                a_t.append(a)
                                f_t.append(ccc)
                        output = f"{next((f_t['address'] for f_t in f_t), 'Not found')}, {a_unique}\n"
                        result_text.insert(tk.END, output)

# 创建主窗口
root = tk.Tk()
root.title("旭旭小仙女的工具箱")

# 使用网格布局调整窗口大小
root.grid_columnconfigure(1, weight=1)
root.grid_rowconfigure(4, weight=1)

# 接入号标签和文本域
access_number_label = tk.Label(root, text="接入号(用逗号分隔):")
access_number_label.grid(column=0, row=0, padx=10, pady=10, sticky="N")

access_number_text = tk.Text(root, height=5, width=50)
access_number_text.grid(column=1, row=0, padx=10, pady=10, sticky="EW")

# Cookie标签和文本域
cookie_label = tk.Label(root, text="Cookie:")
cookie_label.grid(column=0, row=1, padx=10, pady=10, sticky="N")

cookie_text = tk.Text(root, height=5, width=50)
cookie_text.grid(column=1, row=1, padx=10, pady=10, sticky="EW")

# 提交按钮
submit_button = tk.Button(root, text="提交", command=submit)
submit_button.grid(column=1, row=2, padx=10, pady=10, sticky="EW")

# 结果显示区域
result_label = tk.Label(root, text="结果:")
result_label.grid(column=0, row=3, padx=10, pady=10, sticky="N")

result_text = scrolledtext.ScrolledText(root)
result_text.grid(column=0, row=4, columnspan=2, padx=10, pady=10, sticky="NSEW")

# 启动主循环
root.mainloop()
